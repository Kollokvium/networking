import Foundation
import Moya

struct NetworkLayer {
    func getPhotoModel(completion: @escaping (_ data: [MainPhotoModel]) -> ()) {
        let provider = MoyaProvider<PhotoDomainService>()
        provider.request(.showAllPhotos) { result in
            switch result {
            case .success(let success):
                do {
                    let filteredResponse = try success.filterSuccessfulStatusCodes()
                    let photoModel = try filteredResponse.map([MainPhotoModel].self)
                    completion(photoModel)
                } catch {
                    print(error.localizedDescription)
                    print(success.statusCode)
                }
            case .failure(let failure):
                print(failure)
            }
        }
    }
}
