import Foundation
import Moya
import Alamofire

enum PhotoDomainService {
    case showAllPhotos
    case likePhoto(id: Int)
}

// MARK: - TargetType Protocol Implementation
extension PhotoDomainService: TargetType {
    var baseURL: URL {
        guard let url = URL(string: "https://api.unsplash.com") else { fatalError() }
        return url
    }
    
    var path: String {
        switch  self {
        case .showAllPhotos:
            return "/photos"
        case .likePhoto(let id):
            return "/photos/:\(id)/like"
        }
    }
    
    var method: Moya.Method {
        switch  self {
        case .showAllPhotos:
            return .get
        case .likePhoto:
            return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch  self {
        case .showAllPhotos:
            return .requestParameters(parameters: ["client_id" : "61178b6f52ce434f2045e9cad5c5632736df403b338ef03dfaafb03f4248238b"],
                                      encoding: URLEncoding.queryString)
        case .likePhoto(let id):
            return .requestParameters(parameters: ["photos" : id],
                                      encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-type"   : "application/json",
                "Accept-Version" : "v1"]
    }
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
