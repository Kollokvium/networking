protocol ScreenProtocol {
    func setupHierarchy()
    func setupLayout()
    func setupStyle()
    func setupContent()
    func setupObserving()
}
