import UIKit
import Moya
import Kingfisher

class ViewController: UIViewController, ScreenProtocol {
    
    @IBOutlet weak var phototableView: UITableView!
    var photoModel: [MainPhotoModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupHierarchy()
        setupLayout()
        setupStyle()
        setupContent()
        setupObserving()
        getPhotoModels()
    }
    
    func getPhotoModels() {
        NetworkLayer().getPhotoModel { photoModel in
            self.photoModel = photoModel
            self.phototableView.reloadData()
        }
    }
}

// MARK: - DataSource & Delegate
extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photoModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoTableViewCell") as? PhotoTableViewCell else { return UITableViewCell() }
        
        cell.likesLabel.text = String(photoModel[indexPath.row].likes)
        cell.userLabel.text = photoModel[indexPath.row].user.name
        cell.photoView?.kf.indicatorType = .activity
        cell.photoView?.kf.setImage(with: try?
            photoModel[indexPath.row]
                .urls
                .regular
                .asURL())
        return cell
    }
}

// MARK: - Setup Layout
extension ViewController {
    func setupHierarchy() {
        
    }
    
    func setupLayout() {
        phototableView.reloadData()
    }
    
    func setupStyle() {
        phototableView.rowHeight = 300
        phototableView.separatorInset.left = 20
        phototableView.separatorInset.right = 20
    }
    
    func setupContent() {
        title = "Unsplash Photos"
    }
    
    func setupObserving() {
        phototableView.delegate = self
        phototableView.dataSource = self
    }
}
