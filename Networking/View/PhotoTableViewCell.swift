import UIKit

class PhotoTableViewCell: UITableViewCell {

    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var photoTextDetails: UIView!
    
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var userLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        setupCellStyle()
    }
    
    func setupCellStyle() {
        photoView.layer.cornerRadius = 6
        photoView.clipsToBounds = true
        photoTextDetails.layer.cornerRadius = 6
        photoTextDetails.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        photoTextDetails.clipsToBounds = true
        
        userLabel.textColor = #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1)
        likesLabel.textColor = #colorLiteral(red: 1, green: 0.2527923882, blue: 1, alpha: 1)
        photoView.contentMode = .scaleAspectFill
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
