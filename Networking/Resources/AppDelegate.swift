import UIKit
import Moya
import Result

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setupThirdPartyServices()
        setupUI()
        
        return true
    }
    
    private func setupThirdPartyServices() {
        
    }
    
    private func setupUI() {
        UINavigationBar.appearance().barStyle = .blackOpaque
    }
}
