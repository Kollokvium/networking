import Foundation

struct MainPhotoModel: Decodable {
    let id: String
    let createdAt: String
    let updatedAt: String
    let width: Int
    let height: Int
    let color: String
    let likes: Int
    let linkedByUser: Bool
    let description: String?
    let user: User
    let currentUserCollections: [CurrentUserCollections]
    let urls: Urls
    let links: Links
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case width = "width"
        case height = "height"
        case color = "color"
        case likes = "likes"
        case linkedByUser = "liked_by_user"
        case description = "description"
        case user = "user"
        case currentUserCollections = "current_user_collections"
        case urls = "urls"
        case links = "links"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        createdAt = try values.decode(String.self, forKey: .createdAt)
        updatedAt = try values.decode(String.self, forKey: .updatedAt)
        width = try values.decode(Int.self, forKey: .width)
        height = try values.decode(Int.self, forKey: .height)
        color = try values.decode(String.self, forKey: .color)
        likes = try values.decode(Int.self, forKey: .likes)
        linkedByUser = try values.decode(Bool.self, forKey: .linkedByUser)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        user = try values.decode(User.self, forKey: .user)
        currentUserCollections = try values.decode([CurrentUserCollections].self, forKey: .currentUserCollections)
        urls = try values.decode(Urls.self, forKey: .urls)
        links = try values.decode(Links.self, forKey: .links)
    }
}

struct CurrentUserCollections: Decodable {
    let id: Int
    let title: String
    let publishedAt: String
    let updatedAt: String
    let curated: Bool
    let coverPhoto: String
    let user: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case publishedAt = "published_at"
        case updatedAt = "updated_at"
        case curated = "curated"
        case coverPhoto = "cover_photo"
        case user = "user"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        title = try values.decode(String.self, forKey: .title)
        publishedAt = try values.decode(String.self, forKey: .publishedAt)
        updatedAt = try values.decode(String.self, forKey: .updatedAt)
        curated = try values.decode(Bool.self, forKey: .curated)
        coverPhoto = try values.decode(String.self, forKey: .coverPhoto)
        user = try values.decode(String.self, forKey: .user)
    }
}

struct Links: Decodable {
    let selfy: String?
    let html: String?
    let download: String?
    let downloadLocation: String?
    
    enum CodingKeys: String, CodingKey {
        case selfy = "self"
        case html = "html"
        case download = "download"
        case downloadLocation = "download_location"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        selfy = try values.decodeIfPresent(String.self, forKey: .selfy)
        html = try values.decodeIfPresent(String.self, forKey: .html)
        download = try values.decodeIfPresent(String.self, forKey: .download)
        downloadLocation = try values.decodeIfPresent(String.self, forKey: .downloadLocation)
    }
}

struct ProfileImage: Decodable {
    let small: String
    let medium: String
    let large: String
    
    enum CodingKeys: String, CodingKey {
        case small = "small"
        case medium = "medium"
        case large = "large"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        small = try values.decode(String.self, forKey: .small)
        medium = try values.decode(String.self, forKey: .medium)
        large = try values.decode(String.self, forKey: .large)
    }
}

struct Urls: Decodable {
    let raw: String
    let full: String
    let regular: String
    let small: String
    let thumb: String
    
    enum CodingKeys: String, CodingKey {
        case raw = "raw"
        case full = "full"
        case regular = "regular"
        case small = "small"
        case thumb = "thumb"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        raw = try values.decode(String.self, forKey: .raw)
        full = try values.decode(String.self, forKey: .full)
        regular = try values.decode(String.self, forKey: .regular)
        small = try values.decode(String.self, forKey: .small)
        thumb = try values.decode(String.self, forKey: .thumb)
    }
}

struct User: Decodable {
    let id: String
    let username: String?
    let name: String?
    let portfolioUrl: String?
    let bio: String?
    let location: String?
    let totalLikes: Int?
    let totalPhotos: Int?
    let totalCollections: Int?
    let instagramUsername: String?
    let twitterUsername: String?
    let profileImage: ProfileImage?
    let links: Links?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case username = "username"
        case name = "name"
        case portfolioUrl = "portfolio_url"
        case bio = "bio"
        case location = "location"
        case totalLikes = "total_likes"
        case totalPhotos = "total_photos"
        case totalCollections = "total_collections"
        case instagramUsername = "instagram_username"
        case twitterUsername = "twitter_username"
        case profileImage = "profile_image"
        case links = "links"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        portfolioUrl = try values.decodeIfPresent(String.self, forKey: .portfolioUrl)
        bio = try values.decodeIfPresent(String.self, forKey: .bio)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        totalLikes = try values.decodeIfPresent(Int.self, forKey: .totalLikes)
        totalPhotos = try values.decodeIfPresent(Int.self, forKey: .totalPhotos)
        totalCollections = try values.decodeIfPresent(Int.self, forKey: .totalCollections)
        instagramUsername = try values.decodeIfPresent(String.self, forKey: .instagramUsername)
        twitterUsername = try values.decodeIfPresent(String.self, forKey: .twitterUsername)
        profileImage = try values.decodeIfPresent(ProfileImage.self, forKey: .profileImage)
        links = try values.decodeIfPresent(Links.self, forKey: .links)
    }
}
