import Quick
import Nimble
@testable import Networking

class NetworkingTests: QuickSpec {
    override func spec() {
        var subject = ViewController()
        describe("NetworkingTests") {
            beforeEach {
                subject = UIStoryboard(name: "Main", bundle: nil)
                    .instantiateViewController(withIdentifier: "ViewController") as! ViewController
                _ = subject.view
            }
            
            context("when view is loaded") {
                it("should have 10 pictures") {
                    expect(subject.phototableView.numberOfRows(inSection: 0)).to(equal(subject.photoModel.count))
                }
            }
            
            context("Navigation Controller Text") {
                it("should not have any text") {
                    expect(subject.navigationController?.title).to(beNil())
                }
            }
            
            context("Check Initial Stat of Photo Model Array") {
                it("should be zero models") {
                    expect(subject.photoModel.count).to(equal(0))
                }
            }
        }
    }
}
